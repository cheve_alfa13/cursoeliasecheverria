package com.disney.projects;

import java.util.Scanner;

public class ScannerOperations {
	
	public static void product(int a, int b) {
		
		System.out.println("Product: " + (a * b));
	 
	}

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Give A please:");
		int a = scanner.nextInt();

		System.out.println("Give B please:");
		int b = scanner.nextInt();

		product(a, b);
	}

}

package com.disney.projects;
/**
 * @author EliasAvila
  * --------------------------------------------------------------------------------------------------
* Elias Echeverria Avila
 * Date: Friday 14 th September of 2018
Exercise: Make a program  with switch case simple
--------------------------------------------------------------------------------------------------
*/

public class SwichtAndCase {

	public static void main (String []args) {
		int x;
		x=1;
		switch (x) {
		case 0:
		{
			System.out.println("esta es la opcion 0");
			break;
		}
		case 1:{
			System.out.println("esta es la opcion 1");
			break;
		}
		default:{
			System.out.println("Esta opcion es la opcion por defecto");
		}
		}
	}
}

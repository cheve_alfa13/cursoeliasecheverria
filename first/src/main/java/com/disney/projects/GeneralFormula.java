package com.disney.projects;
import java.util.Scanner;
public class GeneralFormula {

	public static void main(String[] args) {
		Scanner t = new Scanner(System.in);
		double a, b, c = 0;
		double x1, x2 = 0;
		double d = 0;
		System.out.println("Give the coefficient de A");
		a = t.nextInt();
		System.out.println("Give the coefficient de B");
		b = t.nextInt();
		System.out.println("Give the coefficient de C");
		c = t.nextInt();
		d = Math.pow(b, 2) - 4 * a * c;
		if (d < 0) {
			System.out.println("La solution not is real");

		} else {
			x1 = (-b + Math.sqrt(d)) / 2 * a;
			x2 = (-b + Math.sqrt(d)) / 2 * a;
			System.out.println();
			System.out.printf("The solution is:\nx1: %f\nx2: %f", x1, x2);

		}

	}

}
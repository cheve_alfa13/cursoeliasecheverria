package com.disney.projects;
/***
 * --------------------------------------------------------------------------------------------------
 * Elias Echeverria Avila
 * Date: Friday 14 th September of 2018
 Exercise: make a program, that the user enter numbers and that the program validates, how many digits enter.
 --------------------------------------------------------------------------------------------------
 */
import java.util.Scanner;

public class ifelse {
	public static void main(String[] args) {

		int num;

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a four digit number ");

		num = sc.nextInt();

		if (num > 0 && num < 10) {

			System.out.println("The numbres is one digit");

		} else {
			if (num >= 10 && num < 100) {

				System.out.println("The number is two digit");

			} else {

				if (num >= 100 && num < 1000) {

					System.out.println("The number is three digit");

				} else {

					if (num >= 1000 && num < 10000) {

						System.out.println("The number is four digit");

					}
				}
			}
		}
	}

}

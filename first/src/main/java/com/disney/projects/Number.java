package com.disney.projects;
/**
--------------------------------------------------------------------------------------------------
 
 * Elias Echeverria Avila
  * Date: Friday 14 th September of 2018
Exercise; Make a program, enter a number and validate if the number entered is positive or negative
--------------------------------------------------------------------------------------------------
 */
import java.util.Scanner;

public class Number {

	public static void main(String[] args) {

		int number = 0;

		Scanner read = new Scanner(System.in);

		System.out.println("Write a  number ");

		number = read.nextInt();

		if (number > 0) {

			System.out.println("Number is positive.");

		} else if (number == 0) {

			System.out.println("This number is neutral");

		} else {

			System.out.println("Number is not positive.");

		}
	}
}
